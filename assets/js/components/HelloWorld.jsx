import React from 'react'

export default class HelloWorld extends React.Component {
    constructor(props) {
        super(props)
        this._onClick = this._onClick.bind(this)
    }
    
    _onClick() {
        const newMessage = "Yes I am :)"
        this.props.onClick(newMessage)
    }

    render(){
        return (
            <div className="hello-world">
                <p>Hello {this.props.message}!</p>
                <p>{this.props.storeMessage.message}</p>
                <p>
                    <button type="button" onClick={this._onClick}>Try me</button>
                </p>
            </div>
        )
    }
}