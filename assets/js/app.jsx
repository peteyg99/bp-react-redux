import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import store from './store'

import HelloWorld from './containers/HelloWorld'

(function() {
    "use strict"
    
    var App = React.createClass({
        render: function() {
            return (
                <div>
                    <h1>BoilerPlate - React</h1>
                    <HelloWorld message="world" />
                </div>
            )
        }
    })

    const appRoot = document.getElementById("root")

    ReactDOM.render(
        <Provider store={store}><App /></Provider>, 
        appRoot)
})()