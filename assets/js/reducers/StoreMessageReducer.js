import { UPDATE_MESSAGE } from '../constants'

export default function storeMessageReducer(state = {}, action) {
    switch (action.type) {
        case UPDATE_MESSAGE:
            return {
                ...state,
                message: action.newStoreMessage
            }
        default:
            return state
    }
}
