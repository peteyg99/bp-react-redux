import { combineReducers } from 'redux'

// Reducers
import storeMessageReducer from './StoreMessageReducer'

// Combine Reducers
export default combineReducers({
        storeMessage: storeMessageReducer,
})
