import { UPDATE_MESSAGE } from '../constants'

export const updateStoreMessage = (newStoreMessage) => {

    return (dispatch) => {

        dispatch({ 
            type: UPDATE_MESSAGE,
            newStoreMessage: newStoreMessage
        })
    }

}